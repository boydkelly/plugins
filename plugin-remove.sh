#!/usr/bin/bash
[[ -z $1 ]] && { echo need plugin ; exit 1 ; }
[[ ! -d $1 ]] && { echo need plugin ; exit 1 ; }
#plugin=dirname(%1)
echo $1
#[[ -d %1 ]] || exit 1
git commit -a -m "$(date -Ih) before removing $1" 
git submodule deinit -f $1
#need to remove trailing slash here
git rm --cached -f $1
rm -frv $1
rm -frv .git/modules/$1
#git commit --quiet -a -m "$(date -Ih) Removed %1" 
git commit -a -m "$(date -Ih) Removed $1" 
