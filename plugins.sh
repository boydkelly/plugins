#!/usr/bin/bash
#first update, then produce list of installed submodules csv delimited and sorted

git submodule update --remote && git commit --quiet -a -m '$(date)' 
export LC_ALL=C
git submodule | awk 'BEGIN { OFS="," } { print $2, $3}' | sed 's/t\//t,/g' | sort -t, -k2 > current.csv
#cat description.csv | sort -t, -k2 > tmp
#mv tmp description.csv

join -t, -a 1 -1 2 -2 2 -o 1.1,1.2,1.3,2.4,2.5 current.csv description.csv | sort -u -t, -k1,2 | tee nvim-plugins.csv | sort -u -t, -k2 > tmp 
mv tmp description.csv

cp nvim-plugins.csv ~/dev/web-prod/content/include/
pushd ~/dev/web-prod
git commit --quiet content/include/nvim-plugins.csv -m "'$(date)' update nvim-plugins" && git push origin master --quiet 
popd

#| sort > nvim-plugins.csv
#take the first 3 fields including verion from file 1 but the note and descriptions from file 2
#join -t, -a 1 -1 2 -2 2 <(sort current.csv) legacy.csv -o 1.1,1.2,1.3,2.4,2.5 | sort > nvim-plugins.csv
#now update the legacy file with any new records
#cat legacy.csv | sort -k 1,2 > tmp.csv
#join -t, -a 1 -1 2 -2 2 <(sort tmp.csv) nvim-plugins.csv -o 1.1,1.2,1.3,2.4,2.5 | sort -k 1,2 > legacy.csv

git commit --quiet -a -m "$(date -Im)" && git push origin master --quiet

