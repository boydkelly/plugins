#!/usr/bin/bash
MASTER=legacy.csv
INPUT=current.csv
awk 'BEGIN {FS=OFS=","} NR==FNR{a[$2]=$2;next} $2 in a{$2=a[$2];print $1 $3 $5}' legacy.csv current.csv
awk 'BEGIN {FS=OFS=","} NR==FNR{a[$2]=$2;next} $2 in a{$2=a[$2];print $0}' current.csv legacy.csv
awk '{a[$1]=$0}END{for(i in a)print a[i]}' ${INPUT} ${MASTER} | sort
